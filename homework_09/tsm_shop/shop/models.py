from django.db import models
from django.contrib.auth.models import User as nickname

class User(models.Model):
    FirstName = models.CharField(max_length=100)
    LastName = models.CharField(max_length=100)

    def __str__(self):
        return f"User <{self.id}, {self.FirstName} {self.LastName}>"



class Product(models.Model):
    name = models.CharField(max_length=20)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    added = models.ForeignKey(nickname, on_delete=models.CASCADE)

    def __str__(self):
        return f"Product <{self.pk}, {self.name!r}>"
