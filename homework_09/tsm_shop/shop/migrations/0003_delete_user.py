# Generated by Django 4.2.4 on 2023-08-18 14:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_alter_product_added'),
    ]

    operations = [
        migrations.DeleteModel(
            name='User',
        ),
    ]
