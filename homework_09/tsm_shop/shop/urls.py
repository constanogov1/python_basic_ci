from django.urls import path
from .views import (
    TSMIndexView,
    ProductListView,
    ProductCreateView,
    ProductDetailView,
)

app_name = "shop"

urlpatterns = [
    path("", TSMIndexView.as_view(), name="index"),
    path("products/", ProductListView.as_view(), name="products"),
    path("new_products/", ProductCreateView.as_view(), name="new_products"),
    path("products/<int:pk>/", ProductDetailView.as_view(), name="product"),
]
