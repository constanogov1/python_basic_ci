from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from .models import Product
from django.views.generic import TemplateView, ListView, CreateView, DetailView
from django.urls import reverse_lazy
from .forms import ProductForm
class TSMIndexView(TemplateView):
    template_name = "shop/index.html"


class ProductListView(ListView):
    template_name = "shop/products.html"
    context_object_name = "products"
    queryset = Product.objects.order_by("id").select_related("added").all()

class ProductCreateView(CreateView):
    template_name = "shop/new_products.html"
#    model = Product
    form_class = ProductForm
#   fields = "name", "price", "description", "added"
    success_url = reverse_lazy("shop:products")

class ProductDetailView(DetailView):
#    template_name = "product_detail.html"
    model = Product