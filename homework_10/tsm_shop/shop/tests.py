from typing import Type
from django.test import TestCase
from django.urls import reverse
from http import HTTPStatus
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from .models import User as model_User
UserModel: Type[AbstractUser] = get_user_model()

class IndexViewTestCase(TestCase):

    def test_index_view_status_ok(self):
        url = reverse("shop:index")
        response = self.client.get(url)
        self.assertEquals(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed((response, "shop/index.html"))


class ProductsViewTestCase(TestCase):
    def setUp(self) -> None:
        self.username = "test"
        self.password = "testpwd__!34"
        self.user: AbstractUser = UserModel.objects.create_user(
            username=self.username,
            password=self.password,
        )
        url = reverse("shop:products")
    def test_anonim_access_no(self):
        url = reverse("shop:products")
        response = self.client.get(url)
        self.assertEquals(response.status_code, HTTPStatus.FOUND)

    def test_login_accesss_ok(self):
        self.client.login(
            username=self.username,
            password=self.password,
        )
        url = reverse("shop:products")
        response = self.client.get(url)
        self.assertEquals(response.status_code, HTTPStatus.OK)

class UserTestListProducts(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.user = model_User.objects.create(
        FirstName = "Petrtestovich",
        LastName = "Pertovtestov"
        )
    @classmethod
    def tearDownClass(cls):
        cls.user.delete()
    def test_get_user(self):
        # count = model_User.objects.count()
        # self.assertEquals(count, 1)
        user = model_User.objects.first()
        self.assertEquals(user.pk, self.user.pk)

class UseristViewTestCase(TestCase):
    fixtures = ["user-fixtures.json"]

    def test_ok(self):
        pass