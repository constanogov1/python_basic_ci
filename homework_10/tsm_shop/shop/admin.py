from django.contrib import admin
from .models import Product, User
@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = "id", "name", "added"
    list_display_links = "id", "name"
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = "id", "FirstName", "LastName"
    list_display_links = "id", "FirstName", "LastName"

