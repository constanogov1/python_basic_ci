from django.db import models

class User(models.Model):
    FirstName = models.CharField(max_length=100)
    LastName = models.CharField(max_length=100)

    def __str__(self):
        return f"Blog <{self.id}, {self.FirstName} {self.LastName}>"
class Blog(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    created_date = models.DateTimeField()
    updated_date = models.DateTimeField(auto_now=True)
    published_date = models.DateTimeField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"Blog <{self.id}, {self.title!r}>"