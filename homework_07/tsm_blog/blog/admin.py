from django.contrib import admin
from .models import Blog, User
@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display = "id", "title", "published_date", "author"
    list_display_links = "id", "title"
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = "id", "FirstName", "LastName"
    list_display_links = "id", "FirstName", "LastName"