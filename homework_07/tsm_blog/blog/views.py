from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from .models import Blog
def blog_index(request: HttpRequest) -> HttpResponse:
    blog = Blog.objects.order_by("id").all()
    return render(
        request=request,
        template_name="blog/index.html",
        context={
            "posts": blog,
        }
    )