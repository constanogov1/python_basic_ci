import os
from sqlalchemy import Column, Integer, String, Text, ForeignKey
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker, declared_attr, relationship

DB_APP_PREFIX='home_'

PG_CONN_URI = (
    os.environ.get("SQLALCHEMY_PG_CONN_URI")
    or "postgresql+asyncpg://postgres:password@localhost/postgres"
)

async_engin = create_async_engine(
    PG_CONN_URI,
)

class Base:
    @declared_attr
    def __tablename__(cls):
        return f"{DB_APP_PREFIX}{cls.__name__.lower()}s"

    @declared_attr
    def id(self):
        return Column(Integer, primary_key=True)

Base = declarative_base(cls=Base)
Session = sessionmaker(
    async_engin,
    expire_on_commit=False,
    class_=AsyncSession,
)

class User(Base):
    name = Column(String(30), unique=False, nullable=False)
    username = Column(String(30), unique=True, nullable=False)
    email = Column(String(150), unique=True, nullable=True)

    posts = relationship(
        "Post",
        back_populates="user",
        uselist=True,
    )

class Post(Base):
    user_id = Column(
        Integer,
        ForeignKey("home_users.id"),
        unique=False,
        nullable=False,
    )
    title = Column(String(90), nullable=False)
    body = Column(Text, nullable=False, default="", server_default="")

    user = relationship(
        "User",
        back_populates="posts",
        uselist=False,
    )
