import asyncio
from dataclasses import dataclass
from aiohttp import ClientSession

USERS_DATA_URL = "https://jsonplaceholder.typicode.com/users"
POSTS_DATA_URL = "https://jsonplaceholder.typicode.com/posts"

@dataclass
class Userjson:
    name: str
    username: str
    email: str

@dataclass
class Postjson:
    user_id: int
    title: str
    body: str

async def fetch_json(url: str, data_type: str) -> list[dict]:
    async with ClientSession() as session:
        async with session.get(url) as response:
            data: list[dict] = await response.json()
            return data

async def get_users_list(url: str, data_type: str) -> list[Userjson]:
    jsons_list: list[dict] = await fetch_json(url, data_type)
    users_list = [
        Userjson(
            name=json_user["name"],
            username=json_user["username"],
            email=json_user["email"],
        )
        for json_user in jsons_list
    ]
    return users_list

async def get_posts_list(url: str, data_type: str) -> list[Postjson]:
    jsons_list: list[dict] = await fetch_json(url, data_type)
    posts_list = [
        Postjson(
            user_id=json_post["userId"],
            title=json_post["title"],
            body=json_post["body"],
        )
        for json_post in jsons_list
    ]
    return posts_list


async def main():
    users, posts = await asyncio.gather(
        get_users_list(USERS_DATA_URL, f"{USERS_DATA_URL}".split("/")[-1]),
        get_posts_list(POSTS_DATA_URL, f"{POSTS_DATA_URL}".split("/")[-1]),
    )
    return users, posts


if __name__ == "__main__":
    asyncio.run(main())
