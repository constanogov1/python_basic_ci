import asyncio

from sqlalchemy.ext.asyncio import AsyncSession

from homework_04.models import Base, async_engin, User, Post
from jsonplaceholder_requests import main as get_users_and_posts
from jsonplaceholder_requests import Userjson, Postjson
from models import Session

async def init_tables():
    async with async_engin.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
    async with async_engin.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

async def create_user(session: AsyncSession, user: Userjson) -> User:
    user = User(
        name=user.name,
        username=user.username,
        email=user.email,
    )
    session.add(user)

    try:
        await session.commit()
    except Exception as e:
        pass
    return user

async def create_users(local_session: Session, users: list[Userjson]):
    async with local_session() as session:
        for user in users:
            await create_user(session, user)

async def create_post(session: AsyncSession, post: Postjson) -> Post:
    post = Post(
        user_id=post.user_id,
        title=post.title,
        body=post.body,
    )
    session.add(post)
    await session.commit()

    return post

async def create_posts(local_session: Session, posts: list[Postjson]):
    async with local_session() as session:
        for post in posts:
            await create_post(session, post)

async def async_main():
    async with Session() as session:
        await init_tables()
    users_objs, posts_objs = await get_users_and_posts()

    await create_users(Session, users_objs),
    await create_posts(Session, posts_objs)

def main():
    asyncio.run(async_main())

if __name__ == "__main__":
    main()
